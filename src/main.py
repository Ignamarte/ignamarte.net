import os, re, time, shutil
from jinja2 import Environment, FileSystemLoader, select_autoescape

TEMPLATES_FOLDER = "src/templates/"
RESSOURCES_FOLDER = "src/ressources/"
BUILD_FOLDER = "build"

# Get template files
files = os.listdir(TEMPLATES_FOLDER)
html_files = list(
  filter(lambda e: e if re.search(".*\.html$", e) else None, files)
)

# Create build folder
try:
  shutil.rmtree(BUILD_FOLDER)
  time.sleep(1)
except FileNotFoundError:
  pass

# Copy static files
shutil.copytree(RESSOURCES_FOLDER, BUILD_FOLDER)

# Generate HTML
loader = FileSystemLoader(TEMPLATES_FOLDER)
env = Environment(
  loader=loader,
  autoescape=select_autoescape(['html'])
)

for file in html_files:
  
  template = env.get_template(file)
  html = template.render()
  
  filename = os.path.join(BUILD_FOLDER, file)
  with open(filename, "w") as f:
    f.write(html)