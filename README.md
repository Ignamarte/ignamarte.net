# Ignamarte.net

My personal website.

## Build
Download the sources, install python the latest version of pyhton 3
with pip, install the requirements and run the script :

```sh
git clone <repo_url_without_chevrons>
cd ignamarte.net
pip install -r requirements
python src/main.py
```

The generated html is located in the `build` folder.